<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;

/**
 * Class AbstractController
 */
class AbstractController extends Controller {
	
	/**
	 * Get Doctrine entity manager.
	 * @return EntityManager
	 */
	protected function getDm() {
		return $this->get("doctrine")->getManager();
	}
	
	/**
	 * Get Request.
	 * @return Request
	 */
	protected function getRq() {
		return $this->get('request_stack')->getCurrentRequest();
	}
	
	/**
	 * Get parameter from config/services.yml file.
	 * @param string $key
	 * @param mixed  $default Default null.
	 * @return mixed
	 */
	protected function getParam($key, $default = NULL) {
		if ($this->container->hasParameter($key)) {
			return $this->container->getParameter($key);
		}
		
		return $default;
	}
	
}
