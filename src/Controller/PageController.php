<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class PageController
 */
class PageController extends AbstractController {
	
	/**
	 * List search results.
	 * @return array
	 *
	 * RouteName: app_page_search
	 * @Route("/")
	 * @Template
	 */
	public function searchAction() {
		return [
			'defaultText' => $this->getParam('default_searched_text'),
			'tree'        => $this->getKeywordTree(),
		];
	}
	
	/**
	 * Get the formatted tree of keywords.
	 * @return array
	 */
	protected function getKeywordTree() {
		$tree   = $this->getParam('keyword_tree');
		$return = [];
		
		foreach ($tree as $key => $value) {
			$return[] = $this->processTree($key, $value);
		}
		
		return $return;
	}
	
	/**
	 * Recursively process the config tree.
	 * @param string            $key
	 * @param string|array|null $value
	 * @return array
	 */
	protected function processTree($key, $value) {
		// Result node.
		$node = [];
		
		// Transform string value to array if has comma.
		if (is_string($value) && strpos($value, ",") !== FALSE) {
			$value = explode(",", $value);
		}
		// There are children below.
		if (is_array($value) && count($value)) {
			$node['label'] = $key;
			// Process children.
			foreach ($value as $childKey => $childValue) {
				// Transform child string value to array if has comma.
				if (is_string($childValue) && strpos($childValue, ",") !== FALSE) {
					$childValue = explode(",", $childValue);
				}
				// Child is array.
				if (is_array($childValue)) {
					$node['children'][] = $this->processTree($childKey, $childValue);
				}
				// Child is simple string.
				elseif (is_string($childValue)) {
					$node['children'][] = ['label' => trim($childValue)];
				}
				// No child at all.. the key will be the label.
				else {
					$node['children'][] = ['label' => $childKey];
				}
			}
		}
		// String value becomes a standalone child.
		elseif (is_string($value)) {
			$node['children'] = ['label' => trim($value)];
		}
		
		return $node;
	}
	
}

