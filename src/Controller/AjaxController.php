<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\FlickrApi\Search;
use App\Service\FlickrApi\Info;

/**
 * Class AjaxController
 */
class AjaxController extends AbstractController {
	
	/** @var Search */
	protected $flickrSearch;
	
	/** @var Info */
	protected $flickrInfo;
	
	/**
	 * PageController constructor.
	 * @param Search $flickrSearch
	 * @param Info   $flickrInfo
	 */
	public function __construct(Search $flickrSearch, Info $flickrInfo) {
		$this->flickrSearch = $flickrSearch;
		$this->flickrInfo   = $flickrInfo;
	}
	
	/**
	 * Get images in json.
	 * @param string $text
	 * @param int    $page
	 * @return JsonResponse
	 *
	 * RouteName: app_ajax_getimages
	 * @Route("/search/{text}/{page}", requirements={"page"="\d+|_page_"})
	 */
	public function getImagesAction($text, $page) {
		$images = $this->flickrSearch->getResults($text, $page);
		
		return new JsonResponse($images);
	}
	
	/**
	 * Get image info.
	 * @param int $imageId
	 * @return JsonResponse
	 *
	 * RouteName: app_ajax_getinfo
	 * @Route("/info/{imageId}", requirements={"imageId"="\d+|_imageId_"})
	 */
	public function getInfoAction($imageId) {
		$info = $this->flickrInfo->getResult($imageId);
		
		return new JsonResponse($info);
	}
	
	
}