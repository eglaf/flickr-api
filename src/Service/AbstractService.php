<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AbstractService
 */
class AbstractService {
	
	/** @var ContainerInterface */
	protected $container;
	
	/**
	 * AbstractService constructor.
	 * @param ContainerInterface $container
	 */
	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}
	
	/**
	 * Get parameter from config/services.yml file.
	 * @param string $key
	 * @param mixed  $default Default null.
	 * @return mixed
	 */
	protected function getParam($key, $default = NULL) {
		if ($this->container->hasParameter($key)) {
			return $this->container->getParameter("flickr_api_key");
		}
		
		return $default;
	}
	
}