<?php

namespace App\Service\FlickrApi;

use App\Service\AbstractService;

/**
 * Class AbstractFlickrApi
 */
abstract class AbstractFlickrApi extends AbstractService {
	
	/**
	 * Get url from params.
	 * @param array $params
	 * @return string
	 */
	protected function getUrlFromParams(array $params) {
		$encoded_params = [];
		foreach ($params as $k => $v) {
			$encoded_params[] = urlencode($k) . '=' . urlencode($v);
		}
		
		return "https://api.flickr.com/services/rest/?" . implode('&', $encoded_params);
	}
	
}