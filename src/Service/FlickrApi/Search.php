<?php

namespace App\Service\FlickrApi;

/**
 * Class Search
 */
class Search extends AbstractFlickrApi {
	
	/**
	 * Run search query and gives back images in array.
	 * @param string $text
	 * @param int    $page
	 * @return array
	 */
	public function getResults($text, $page = 1) {
		$params         = $this->getBasicParams();
		$params['text'] = $text;
		$params['page'] = $page;
		
		$response      = file_get_contents($this->getUrlFromParams($params));
		$responseArray = unserialize($response);
		
		if ($responseArray['stat'] == 'ok') {
			return $this->getImages($responseArray);
		}
		else {
			throw new \Exception("Cannot connect to Flickr!");
		}
	}
	
	/**
	 * Get default params.
	 * @return array
	 */
	protected function getBasicParams() {
		return [
			'api_key'  => $this->getParam('flickr_api_key'),
			'method'   => 'flickr.photos.search',
			'format'   => 'php_serial',
			'per_page' => 100,
		];
	}
	
	/**
	 * Get images from response array.
	 * @param array $responseArray
	 * @return array
	 */
	protected function getImages(array $responseArray) {
		$return = [];
		foreach ($responseArray["photos"]["photo"] as $photo) {
			$return[] = [
				'id'    => $photo['id'],
				'url'   => "http://farm{$photo['farm']}.static.flickr.com/{$photo['server']}/{$photo['id']}_{$photo['secret']}_s.jpg",
				'title' => $photo['title'],
				'owner' => $photo['owner'],
			];
		}
		
		return $return;
	}
	
}