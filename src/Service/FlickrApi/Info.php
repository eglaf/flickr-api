<?php

namespace App\Service\FlickrApi;

/**
 * Class Info
 */
class Info extends AbstractFlickrApi {
	
	/**
	 * Get image info.
	 * @param int $imageId
	 * @return array
	 */
	public function getResult($imageId) {
		$params             = $this->getBasicParams();
		$params['photo_id'] = $imageId;
		
		$response      = file_get_contents($this->getUrlFromParams($params));
		$responseArray = unserialize($response);
		
		if ($responseArray['stat'] == 'ok') {
			return $this->getInfo($responseArray);
		}
		else {
			throw new \Exception("Cannot connect to Flickr!");
		}
	}
	
	/**
	 * Get default params.
	 * @return array
	 */
	protected function getBasicParams() {
		return [
			'api_key' => $this->getParam('flickr_api_key'),
			'method'  => 'flickr.photos.getInfo',
			'format'  => 'php_serial',
		];
	}
	
	/**
	 * Get info from response.
	 * @param array $responseArray
	 * @return array
	 */
	protected function getInfo(array $responseArray) {
		$photo = $responseArray['photo'];
		
		$info = [
			'url'         => "http://farm{$photo['farm']}.static.flickr.com/{$photo['server']}/{$photo['id']}_{$photo['secret']}_c.jpg",
			'title'       => $photo['title']['_content'],
			'taken_date'  => $photo['dates']['taken'],
			'user_from'   => $photo['owner']['location'],
			'tags'        => [],
		];
		
		foreach ($photo['tags']['tag'] as $tag) {
			$info['tags'][] = $tag['_content'];
		}
		
		return $info;
	}
	
}
