"use strict";

/**
 * SearchController
 */
var SearchController = function () {

    /** @type {SearchController} */
    var that = this;

    /** @type {string} Ajax search url. */
    this.baseSearchUrl = "";

    /** @type {string} Ajax info url. */
    this.baseInfoUrl = "";

    /** @type {string} Last searched. */
    this.searched = "";

    /** @type {number} Page. */
    this.page = 1;

    /**
     * Set search url.
     * @param baseSearchUrl {string}
     * @return {SearchController}
     */
    this.setBaseSearchUrl = function (baseSearchUrl) {
        that.baseSearchUrl = baseSearchUrl;

        return that;
    };

    /**
     * Set info url.
     * @param baseInfoUrl {string}
     * @return {SearchController}
     */
    this.setBaseInfoUrl = function (baseInfoUrl) {
        that.baseInfoUrl = baseInfoUrl;

        return that;
    };

    /**
     * Set default text.
     * @param defaultText
     * @returns {SearchController}
     */
    this.setDefaultText = function (defaultText) {
        that.searched = defaultText;

        return this;
    };

    /**
     * Initialize.
     * @return {SearchController}
     */
    this.init = function () {
        that.initEvents();

        Egf.Elem.find("#searchedText").value = that.searched;

        that.runAjaxSearch(that.getAjaxSearchUrlWithWord(that.searched));

        return this;
    };

    /**
     * Add events.
     * @return {SearchController}
     */
    this.initEvents = function () {
        // Search event.
        that.initSearchEvent();
        that.initTreeEvents();

        // Hide big image.
        Egf.Elem.addEvent("#img-darkness", "click", function () {
            that.hideBigImage();
        });

        return this;
    };

    /**
     * Init search events.
     * @return {SearchController}
     */
    this.initSearchEvent = function () {
        // Search by input value event.
        Egf.Elem.addEvent("#doSearch", "click", function () {
            Egf.Elem.find("#image-list").innerHTML = "";

            var text = Egf.Elem.find("#searchedText").value;
            if (text !== that.searched) {
                that.searched = text;

                if (that.searched.length > 2) {
                    that.page = 1;

                    that.runAjaxSearch(that.getAjaxSearchUrlWithWord(that.searched));
                }
                else {
                    alert("Please give more than 2 characters!");
                }
            }
        });

        // Load more image event.
        Egf.Elem.addEvent("#searchMore", "click", function () {
            that.page++;

            that.runAjaxSearch(that.getAjaxSearchUrlWithWord(that.searched));
        });

        return that;
    };

    /**
     * Init tree events.
     * @return {SearchController}
     */
    this.initTreeEvents = function () {
        // Toggle tree button.
        Egf.Elem.addEvent("#tree-toggle", "click", function () {
            var tree = Egf.Elem.find("#keyword-tree");
            if (Egf.Elem.hasCssClass(tree, "hidden")) {
                Egf.Elem.removeCssClass(tree, "hidden");
            } else {
                Egf.Elem.addCssClass(tree, "hidden");
            }
        });

        // Search clicked keyword.
        Egf.Elem.addEvent(".keyword", "click", function (event) {
            Egf.Elem.find("#image-list").innerHTML = "";
            Egf.Elem.addCssClass("#keyword-tree", "hidden");

            that.searched = event.target.textContent;
            that.runAjaxSearch(that.getAjaxSearchUrlWithWord(that.searched));

        });

        return that;
    };

    /**************************************************************************************************************************************************************
     *                                                          **         **         **         **         **         **         **         **         **         **
     * Ajax                                                       **         **         **         **         **         **         **         **         **         **
     *                                                          **         **         **         **         **         **         **         **         **         **
     *************************************************************************************************************************************************************/

    /**
     * Gives back the Ajax search url with given word.
     * @param text {string}
     * @returns {string}
     */
    this.getAjaxSearchUrlWithWord = function (text) {
        return new Egf.Sf.Route()
            .setRoute(that.baseSearchUrl)
            .setParams({'_text_': text, '_page_': that.page})
            .getUrl();
    };

    /**
     * Run ajax search with given keyword.
     * @param searchUrl
     */
    this.runAjaxSearch = function (searchUrl) {
        Egf.Ajax.get(searchUrl, {}, function (images) {
            var ul = Egf.Elem.find("#image-list");

            // Add img to dom.
            Egf.Util.forEach(images, function (image) {
                ul.appendChild(that.getListElem(image));
            });

            // Remove existing img click events.
            Egf.Util.forEach(Egf.Elem.find("img"), function (imgElem) {
                imgElem.removeEventListener("click", that.imageClick);
            });

            // Add img click events.
            Egf.Elem.addEvent("img", "click", that.imageClick);
        });
    };

    /**
     * Gives back the Ajax info url.
     * @param imageId {number}
     * @return {string}
     */
    this.getAjaxInfoUrlWithId = function (imageId) {
        return new Egf.Sf.Route()
            .setRoute(that.baseInfoUrl)
            .setParams({'_imageId_': imageId})
            .getUrl();
    };

    /**************************************************************************************************************************************************************
     *                                                          **         **         **         **         **         **         **         **         **         **
     * Elements                                                   **         **         **         **         **         **         **         **         **         **
     *                                                          **         **         **         **         **         **         **         **         **         **
     *************************************************************************************************************************************************************/

    /**
     * Gives back a list item element of the given image.
     * @param image {object}
     * @return {Element}
     */
    this.getListElem = function (image) {
        var img = document.createElement("img");
        img.setAttribute("src", image.url);
        img.setAttribute("data-image-id", image.id);
        var li = document.createElement("li");
        li.appendChild(img);

        return li;
    };

    /**
     * Event of an image click.
     * @param imgClickEvent {Event}
     */
    this.imageClick = function (imgClickEvent) {
        var imageId = imgClickEvent.target.getAttribute("data-image-id");

        Egf.Ajax.get(that.getAjaxInfoUrlWithId(imageId), {}, function (imageData) {
            that.showBigImage(imageData);
        });
    };

    /**
     * Show big image and info.
     * @param imageData
     */
    this.showBigImage = function (imageData) {
        Egf.Elem.find("#img-title").innerHTML = imageData.title;
        Egf.Elem.find("#img-data").innerHTML  = imageData.user_from + " " + imageData.taken_date;
        Egf.Elem.find("#img-image").setAttribute("src", imageData.url);

        var tags = "";
        Egf.Util.forEach(imageData.tags, function (tag) {
            tags += "<span class='tag'>" + tag + "</span>, ";
        });
        Egf.Elem.find("#img-tags").innerHTML = Egf.Util.trim(tags, ", ");

        // Tag click.
        Egf.Elem.addEvent(".tag", "click", function (event) {
            Egf.Elem.find("#image-list").innerHTML = "";

            that.searched = event.target.textContent;
            that.runAjaxSearch(that.getAjaxSearchUrlWithWord(that.searched));
            Egf.Elem.find("#searchedText").value = that.searched;

            that.hideBigImage();
        });

        Egf.Elem.removeCssClass('#img', 'hidden');
    };

    /**
     * Hide big image and info.
     */
    this.hideBigImage = function () {
        Egf.Elem.addCssClass('#img', 'hidden');
    };

};
