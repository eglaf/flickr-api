PHP: Symfony 4 (src/Controller/, src/Service/, public/js/, templates/, config/services.yml)
JS: Sajat fejlesztesu modulgyujtemeny

Vendor mappa telepitesehez composer szukseges. // php composer.phar install
DocumentRoot a public mappa legyen.

Elkeszult funkciok:
- Kereses
- Tovabbi talalatok
- Kepre kattintasra reszletek
- Reszleteknel tag-re kattintasra szur
- Config-bol olvassa a keywords fa tartalmat rekurzivan (config/services.yml, src/Controller/PageController, templates/page/search.html.twig)

Keywords config CRUD-ra nincs lehetoseg.
Ehhez DB-ben lenne celszeru tarolni az adatokat a config file helyett.
Viszont az csak 3 darab nagyon egyszeru method lenne.
Config-bol olvasos feladatat erdekesebbnek tunt.

/*
Sajnalatos modon... jelenlegi (ideiglenes) lakhelyemen NAGYON rossz a net.
Ez a fejlesztest is megnehezitette... nem kicsit.
Alapos tesztre egyszeruen mar nincs turelmem.
Az esetleges benne maradt hibakert es feature-kert elnezest kerek.
*/
